CC=gcc
CFLAGS=-Wall -pedantic-errors -Wall -Wextra -Werror

LIBS =  -I /usr/include/libxml2  -lncursesw -lxml2 -lmenu

all : main

main: main.c game.o parser.o gui.o 
	$(CC) $(CFLAGS) -o main main.c game.o parser.o gui.o $(LIBS)

parser.o: parser.c
	$(CC) -c parser.c

gui.o: gui.c
	$(CC) -c -o gui.o gui.c

game.o: game.c
	$(CC) -c -o game.o game.c

clean:
	rm *o main
