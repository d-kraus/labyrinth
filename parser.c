#include "parser.h"

int count_lines(FILE *s)
{
    int lines = 0;
    char c;
    do
    {
        c = fgetc(s);
        if (c == '\n' || c == EOF)
            lines ++;
    } while (c != EOF);

    return lines;
}

int count_width(FILE *fp)
{
    int clms = 0;

    char c;
    while ((c = fgetc(fp)) != '\n')
        clms ++;

    return clms;
}

char *fill_tiles(FILE *fp, int width, int height)
{

    size_t s =  height * width; 
    char *map = malloc(s);
    memset(map,'I',s);

    char c;
    int pos = 0;
    do
    {
        c = fgetc(fp);
        if ( c != '\n' && c != EOF) {
            map[pos] = c;
            pos++;
        }

        
    } while (c != EOF);

    map[pos] = '\0';

    //fread(map, s, 1, fp);
    return map;
}

Map parse_map(char* file_name)
{
    FILE* fp = fopen(file_name, "r");

    Map map;

    map.height = count_lines(fp);
    fseek(fp, 0, SEEK_SET);
    map.width = count_width(fp);
    fseek(fp, 0, SEEK_SET);
    map.map = fill_tiles(fp, map.width, map.height);

    fclose(fp);

    return map;
}
