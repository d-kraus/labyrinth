#include "gui.h"
#include <stdlib.h>

void draw_borders(WINDOW *screen)
{
    int x,y,i;
    getmaxyx(screen, y, x);

    // 4 corners
    mvwprintw(screen,0,0,"+");
    mvwprintw(screen,y-1,0,"+");
    mvwprintw(screen,y-1,x-1,"+");
    mvwprintw(screen,0,x-1,"+");

    //sides
        for( i=1; i < y-1; ++i) {
        mvwprintw(screen,i,0,"|");
        mvwprintw(screen,i,x-1,"|");
    }

    //top and bottom
    for (i=1; i < x-1; ++i){
        mvwprintw(screen,0,i,"-");
        mvwprintw(screen,y-1,i,"-");
    }
}

void draw_map(WINDOW *screen, Map map)
{
    /* find the offset to draw the map in the middle */
    int max_y, max_x;
    getmaxyx(screen, max_y, max_x);

    int offset_y = (max_y / 2) - (map.height / 2);
    int offset_x = (max_x / 2) - (map.width / 2);

    /* draw it row wise */
    char *s = malloc((map.width +1 )*sizeof(char));
    s[map.width+1] = '\0';

    for (int row = 0;row < map.height ; row++) {
        memcpy(s,map.map +row* map.width, map.width);
        mvwprintw(screen, offset_y + row, offset_x, s);
    }

    free(s);
}