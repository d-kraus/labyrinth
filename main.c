#include "main.h"
#include "game.h"
#include "parser.h"

int main()
{
   
    Map map = parse_map("map");
    run_game(map);
    return 0;
}