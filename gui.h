#ifndef GUI_H
#define GUI_H

#include <ncurses.h>
#include "data.h"
#include <string.h>

void draw_borders(WINDOW *screen);
void draw_map(WINDOW *screen, Map map);

#endif