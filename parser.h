#ifndef PARSER_H
#define PARSER_H

#include "data.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

Map parse_map(char* file_name);

#endif