#include "game.h"
#include "gui.h"
#include <locale.h>
#include <ncurses.h>
#include <time.h>
#include <stdlib.h>


void create_player(Map *map, Player *p)
{

    p->c = 'V';
    int k = 0;
    int pos = 0;
    /*find a empty tile for the player */
    for(int i = 0; i < map->width * map->height; i++) {

        if (map->map[i] == ' ') {
            k++;
            if ( (double)rand() / (double)RAND_MAX  < 1.0 / k)
                pos = i;
            
        }
    }

    p->pos_x = pos % map->width;
    p->pos_y = pos / map->width;
}

/* return value indicates winnig condition */
int move_player(Map *map, Player *p, char dir) 
{
    int new_x;
    int new_y;
    switch(dir)
            {
            case 'A':
                // UP
                new_x = p->pos_x;
                new_y = p->pos_y - 1;
                break;
            case 'B':
                // DOWN
                new_x = p->pos_x;
                new_y = p->pos_y + 1;
                break;
            case 'C':
                // RIGHT
                new_x = p->pos_x + 1;
                new_y = p->pos_y;
                break;
            case 'D':
                // left
                new_x = p->pos_x - 1;
                new_y = p->pos_y;
                break;
            }

    if (new_x < 0 || new_y < 0 || new_y >= map->height || new_x >= map->width)
        return 0;

    /* check if new position is empty */
    int new_pos = new_y * map->width + new_x;
    int old_pos = p->pos_y * map->width + p->pos_x;

    if (map->map[new_pos] != '#') {
        map->map[new_pos] = p->c;
        map->map[old_pos] = ' ';
        p->pos_x = new_x;
        p->pos_y = new_y;
    }

    if (map->map[new_pos] == 'X')
        return 1;

    return 0;

}

void run_game(Map map) 
{
    srand(time(NULL));

    Player p;
    create_player(&map, &p);
    //p.pos_x = 1;
    //p.pos_y = 1;
    map.map[p.pos_y*map.width + p.pos_x] = p.c;

    /* GUI */
    setlocale(LC_CTYPE, "");
    initscr();
    start_color();
    init_pair(1, COLOR_BLACK, COLOR_RED);

    noecho();
    timeout(100);
    curs_set(FALSE);

    int parent_x, parent_y, new_x, new_y, ch;

    /* get our maximum window dimensions */
    getmaxyx(stdscr, parent_y, parent_x);

    /* set up initial windows */
    WINDOW * disp = newwin(parent_y , parent_x, 0 ,0);
    
    draw_map(disp, map); 
    draw_borders(disp);

    /* main loop */
    while ((ch = getch()) != 'q') {
        

        if (ch == '\033'){
            getch(); //skip the [
            switch(getch())
            {
            case 'A':
                // UP
                move_player(&map, &p, 'A');
                break;
            case 'B':
                // DOWN
                move_player(&map, &p, 'B');
                break;
            case 'C':
                // RIGHT
                move_player(&map, &p, 'C');
                break;
            case 'D':
                // left
                move_player(&map, &p, 'D');
                break;
            }
        }       
        getmaxyx(stdscr, new_y, new_x);
        if(new_y != parent_y || new_x != parent_x) {
            parent_x = new_x;
            parent_y = new_y;
            
            wresize(disp, new_y , new_x);
        }

        wclear(disp);
        draw_borders(disp);
        draw_map(disp, map);

        wrefresh(disp);
        refresh();

    }
    
    /* clean up */
    delwin(disp);
    endwin();
}